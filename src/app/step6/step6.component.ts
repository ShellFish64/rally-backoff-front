import { Component, OnInit, HostListener  } from '@angular/core';
import { PersistenceFormsService, AuthenticationService, AlertService } from '@/_services';
import { Router, ActivatedRoute} from '@angular/router';
import { first } from 'rxjs/operators';
import { Rally  } from '@/_models';
import { RallyCRUDrequestsService } from '@/_services/rally-crudrequests.service';
import { BehaviorSubject } from 'rxjs';

@Component({
  selector: 'app-step6',
  templateUrl: './step6.component.html',
  styleUrls: ['./step6.component.scss']
})
export class Step6Component implements OnInit {
  rally: Rally;
  rallyName: string;
  emptyChecker: boolean;
  tempValue: string;
  tempColor: string = "#D52936"; 
  tempMail: string;
  checkerInput: boolean = false;
  userToken: any;
  fullRally: any;
  preventDestroy: boolean = false;

  /**
   * LOCKER.
   * FLAG GENERAL DE VALIDATION EMPECHANT LE SWITCH STEPPING: GuardError.
   */
  guardError = new BehaviorSubject<boolean>(false);

  /**
   * ICI TOUT LES "GUARDS" :
   * FLAGS d'INPUTS desquel DÉPENDENT la VALIDATION des champs et l'AFFICHAGE de MESSAGES.
   * Seul le LOCKER commence a FALSE pour passer a TRUE.
   * Les GAURDS de validation formulaire passent a FALSE si une ERREUR est détectée.
   */
  mailError = new BehaviorSubject<boolean>(undefined);
  nameError = new BehaviorSubject<boolean>(undefined);
  colorError = new BehaviorSubject<boolean>(undefined);
  SpeCharError = new BehaviorSubject<boolean>(undefined);
  keyPressCharError = new BehaviorSubject<boolean>(undefined);

  constructor(public persistenceData: PersistenceFormsService,private router: Router,
    private RALLY_API_GATEWAY: RallyCRUDrequestsService, private authenticationService: AuthenticationService,
    private alertService: AlertService, private route: ActivatedRoute) {
    this.userToken = this.authenticationService.currentUserToken;
  }
  
  @HostListener('window:beforeunload', ['$event'])
  beforeunloadHandler(event) {
    if(this.fullRally.playable !== null) this.save(false);
    if(this.fullRally.playable === null) this.save(true);
    return false;
  }

  ngOnInit() {
    this.rally = this.persistenceData.previousSubmitDataValue[0];
    this.persistenceData.tester();
    console.log(this.rally);
    console.log(this.userToken);
    this.unlocker();
  }

  ngOnDestroy(){
    if(!this.persistenceData.getPreventDestroy && this.fullRally !== undefined && this.fullRally.playable !== null){
      //alert('save from cs6');
      this.save(false);
      console.log('saved from step6 destroy')
    } 
  }

  ngDoCheck(){
    this.fullRally = this.persistenceData.previousSubmitDataValue[2];
    this.rally = this.persistenceData.previousSubmitDataValue[0];
  }

  unlocker(){
    console.log(this.colorError.value, this.mailError.value , this.SpeCharError.value 
      ,this.keyPressCharError.value  );
    /*if(this.colorError.value === undefined && this.mailError.value === undefined && this.SpeCharError.value === undefined 
    && this.keyPressCharError.value === undefined) return this.guardError.next(true);*/
    if(this.mailError.value && this.SpeCharError.value && this.keyPressCharError.value && this.nameError.value) return this.guardError.next(true);
    else return this.guardError.next(false);
  }

  verifyTeamName(name){
    console.log(name);
    let regExName = new RegExp(/[^a-zA-Z0-9 ]/);
    console.log(this.SpeCharError.value);
    console.log(regExName.test(name));

    if(name !== undefined && name.length === 0){
      console.log('empty');
      this.nameError.next(undefined);
      this.SpeCharError.next(undefined);
    }
    if(regExName.test(name)) { 
      this.SpeCharError.next(false);
      console.log(this.SpeCharError.value);
    } else {
      if(name.length === 0) return; 
      this.SpeCharError.next(true)
      console.log(this.SpeCharError.value);
    }
    if(!(name !== undefined && name.length >= 4 && name.length <= 60)){
      this.nameError.next(false);
    } else {
      this.nameError.next(true);
    }
  }

  verifyTeamMail(mail){
    console.log(mail);
    if(mail !== undefined && mail.length === 0){
      this.mailError.next(undefined);
    }
    let regexMail = new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
    if(!(regexMail.test(mail))){
      if(mail.length === 0) return;
      this.mailError.next(false);
    } else {
      this.mailError.next(true); 
    };
  }

  verifyCharsOnPress(){
    let regExName = new RegExp(/[^a-zA-Z0-9 ]/);
    if(!(regExName.test(this.tempValue))){
      return this.keyPressCharError.next(true)
    }
    else return this.keyPressCharError.next(false);
  }


  onSubmit(){
    console.log('hooooo')
    console.log(this.route.snapshot.url);
    console.log(this.route.parent.routeConfig.path);
    if(this.route.snapshot.url[0]['path'] === 'edit-step'){
      console.log('heee')
      this.route.parent.params.pipe(first())
      .subscribe(idObj => {
        let id: string = idObj.id;
        this.edit(id);
        this.persistenceData.activPreventDestroy();
        },
        err => {
          console.log(err);
      });
    }
    if(this.route.parent.routeConfig.path === 'creation-step'){
      console.log('hiiii');
      this.persistenceData.activPreventDestroy();
      this.create();
    }
  }


  edit(id: string){
    console.log('edit');
    this.RALLY_API_GATEWAY.edition(this.fullRally.hashLink, this.persistenceData.previousSubmitDataValue)
    .subscribe(
      rally => {
        //si rally modifié : destruction des données persistente puis retour  sur dashboard
        this.router.navigate(['/dashboard']);
        this.alertService.success('Modification effectué avec succès');
      },
      err => {
        console.log(err);
        this.router.navigate(['/dashboard']);
        this.alertService.error("Erreur au moment de la modification du rally");
      }
    )
  }

  create(){
    console.log('create');
    console.log('create');
    //alert('help');
    this.RALLY_API_GATEWAY.creation(this.persistenceData.previousSubmitDataValue, this.fullRally)
    .pipe(first())
    .subscribe(
      rally => {
        //si rally créé : destruction des données persistente puis retour  sur dashboard
        console.log(rally);
        this.router.navigate(['/dashboard']);
        this.alertService.success("Création du rally connecté effectué avec succès");
      },
      err => {
        console.log(err);
        if(err.status != 403){
          this.router.navigate(['/dashboard']);
          this.alertService.error("Erreur au moment de lupload du média, Essayez a nouveau d'uploader vos media en mode modification du rally");
        }
        this.alertService.error(err.error.message);
        //this.router.navigate(['/dashboard']);
      }
    )
  }


  teamAddor(tempValue: string, tempColor: string, tempMail: string){    
    let tm = {name: tempValue, color: tempColor, mail: tempMail}
    console.log(tm);
    this.rally.teams != null ? this.rally.teams.push(tm) : this.rally.teams = [tm];
    this.tempValue = '';
    this.tempMail = '';
    this.guardError.next(false);
    this.mailError.next(undefined);
    this.nameError.next(undefined);
    this.SpeCharError.next(undefined);
    this.keyPressCharError.next(undefined);
  }

  colorAddor(event: any){
    this.tempColor = event.target.value;
  }
  toggleInput(){
    this.checkerInput = !this.checkerInput;
  }
  remove(team){
    this.rally.teams.splice(this.rally.teams.indexOf(team), 1)
  }

  save(activNav: boolean){
    this.RALLY_API_GATEWAY.storeRally(this.persistenceData.previousSubmitDataValue, this.fullRally)
    .pipe(first())
    .subscribe( result => {
      console.log(result);
      if(activNav){
        this.router.navigate(['dashboard']);
        this.alertService.success('Rally enregistré avec succès');
        return;
      }
    }, err => {
      if(activNav){
        this.alertService.error('Probléme lors de l‘enregistrement du rally !');
        return;
      }
      console.log(err);
    });

  }


}
