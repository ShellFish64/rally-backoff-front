import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { registerLocaleData } from '@angular/common';
import { LOCALE_ID } from '@angular/core';
import localeFr from '@angular/common/locales/fr';
import { appRoutingModule } from './app-routing';
import { AppComponent } from './app.component';
import { RegisterComponent } from './register';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { AlertComponent } from './_components/alert/alert.component';
import { JwtInterceptor } from './_helpers/jwt.interceptor';
import { ErrorInterceptor } from './_helpers/error.interceptor';
import { SlideMenuComponent } from './slide-menu/slide-menu.component';
import { CreationStepComponent } from './creation-step/creation-step.component';
import { Step1Component } from './step1/step1.component';
import { Step2Component } from './step2/step2.component';
import { Step3Component } from './step3/step3.component';
import { Step4Component } from './step4/step4.component';
import { Step5Component } from './step5/step5.component';
import { Step6Component } from './step6/step6.component';
import { AgmCoreModule } from '@agm/core';
import { Ng2ImgMaxModule } from 'ng2-img-max';
import { AngularSvgIconModule } from 'angular-svg-icon';
import { DetailsComponent } from './details/details.component';
import { DashComponent } from './dash/dash.component';
import { ResetpwdComponent } from './resetpwd/resetpwd.component';
import { UpdatePasswordComponent } from './update-password/update-password.component';
registerLocaleData(localeFr, 'fr');

@NgModule({
  imports: [
    BrowserModule,
    appRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    NgbModule,
    FormsModule,
    Ng2ImgMaxModule,
    AgmCoreModule.forRoot({apiKey: 'AIzaSyA1OUZ_uYucR-KrdlaIMsvHjli38488MIc&'}),
    AngularSvgIconModule
  ],
  declarations: [
    AppComponent,
    RegisterComponent,
    LoginComponent,
    HomeComponent,
    AlertComponent,
    SlideMenuComponent,
    CreationStepComponent,
    Step1Component,
    Step2Component,
    Step3Component,
    Step4Component,
    Step5Component,
    Step6Component,
    DetailsComponent,
    DashComponent,
    ResetpwdComponent,
    UpdatePasswordComponent,
  ],
  providers: [
    { provide: LOCALE_ID, useValue: "fr-FR" },
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    //{ provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
