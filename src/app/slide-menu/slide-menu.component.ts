import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '@/_services';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-slide-menu',
  templateUrl: './slide-menu.component.html',
  styleUrls: ['./slide-menu.component.css']
})
export class SlideMenuComponent implements OnInit {
  public activ: boolean;
  public name: string;
  public activate(): Boolean {
    console.log(this.activ);
    return this.activ = !this.activ;   
  }
  constructor(private authenticationService: AuthenticationService, private router: Router, private http: HttpClient) { 
    let token = this.authenticationService.currentUserToken.accessToken;
    let decoded = this.parseJwt(token);
    this.name = decoded.name;
  }

  parseJwt(token) {
    var base64Url = token.split('.')[1];
    var base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
    var jsonPayload = decodeURIComponent(atob(base64).split('').map(function(c) {
        return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
    }).join(''));

    return JSON.parse(jsonPayload);
};

  ngOnInit() {
    this.activ = false;
    console.log(this.activ);
  }

  logout(){
    this.authenticationService.logout();
    this.router.navigate(['login']);
  }

}
