import { TestBed, async, inject } from '@angular/core/testing';

import { PersistenceGuard } from './persistence.guard';

describe('PersistenceGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PersistenceGuard]
    });
  });

  it('should ...', inject([PersistenceGuard], (guard: PersistenceGuard) => {
    expect(guard).toBeTruthy();
  }));
});
