import { Component, OnInit, HostListener } from '@angular/core';
import { PersistenceFormsService, RallyCRUDrequestsService, AlertService } from '@/_services';
import { Router, ActivatedRoute } from '@angular/router';
import { NavigationStepService } from '@/_services';
import { WayPoint, Rally } from '@/_models';
import { first } from 'rxjs/operators';
import { BehaviorSubject } from 'rxjs';

@Component({
  selector: 'app-step3',
  templateUrl: './step3.component.html',
  styleUrls: ['./step3.component.scss']
})
export class Step3Component implements OnInit {
  title: string = 'My first AGM project';
  lat: number;
  lng: number;
  rally: Rally;
  allGpsPoints: WayPoint[] = [];
  state: Boolean = false;
  selectedWayPoint: number;
  infoWindowsOpened = null;
  fullRally: any;
  previousInfoWindow;

  /**
   * LOCKER.
   * FLAG GENERAL DE VALIDATION EMPECHANT LE SWITCH STEPPING: GuardError.
   */
  guardError = new BehaviorSubject<boolean>(false);

  constructor(public persistenceData: PersistenceFormsService, private router: Router, public navigationStepService: NavigationStepService,
    private RALLY_API_GATEWAY: RallyCRUDrequestsService, private alertService: AlertService, private route: ActivatedRoute) {
    console.log(this.persistenceData.previousSubmitDataValue[1]);
  }
  @HostListener('window:beforeunload', ['$event'])
  beforeunloadHandler(event) {
    if (this.fullRally.playable !== null) this.save(false);
    if (this.fullRally.playable === null) this.save(true);
    return false;
  }
  ngOnInit() {
    this.test();
    this.lat = this.persistenceData.previousSubmitDataValue[0]['viewport']['lat'];
    this.lng = this.persistenceData.previousSubmitDataValue[0]['viewport']['lng'];
    this.rally = this.persistenceData.previousSubmitDataValue[0];
    this.fullRally = this.persistenceData.previousSubmitDataValue[2];
    this.allGpsPoints = this.persistenceData.previousSubmitDataValue[1];
    this.initializer();
    console.log(this.lat, this.lng);
  }

  ngOnChanges() {
    console.log('changes onChangeLc');
  }

  ngDoCheck() {
    console.log('checking...');
    if (this.route.snapshot.url[0]['path'] === 'edit-step') {
      //this.lat = this.persistenceData.previousSubmitDataValue[0]['viewport']['lat'];
      //this.lng = this.persistenceData.previousSubmitDataValue[0]['viewport']['lng'];
      //this.rally = this.persistenceData.previousSubmitDataValue[0];
    }
    this.fullRally = this.persistenceData.previousSubmitDataValue[2];
    this.allGpsPoints = this.persistenceData.previousSubmitDataValue[1];
    this.initializer();
    this.analyzeChange();
  }

  ngOnDestroy() {
    if (this.fullRally !== undefined && this.fullRally.playable !== null) {
      this.save(false);
      //alert('saved from 3');
      console.log(this.fullRally);
    }
    console.log(this.lat)
    this.rally.viewport.lat = this.lat,
      this.rally.viewport.lng = this.lng
    console.log(this.rally.viewport)
  }

  onCenterChange($event) {
    console.log($event);
    this.lat = $event.lat,
      this.lng = $event.lng
  }

  /**
   * Initialise les propriétés a null sinon = undefined = erreurs...
   * Uniquement si l'objet est undefined
   * Si WayPoint ajouté/supprimé sur retour d'étape grace a persistence
   * l'initialisatioin ne s'appliquera que sur les objetc undefined donc nouvellement ajoutés... 
   */
  initializer() {
    if (Array.isArray(this.allGpsPoints)) {
      this.allGpsPoints.forEach(WayPoint => {
        if (WayPoint.quizz === undefined) {
          WayPoint.quizz = {
            ask: null,
            answer1: {
              answerBody: null,
              value: false
            },
            answer2: {
              answerBody: null,
              value: false
            },
            answer3: {
              answerBody: null,
              value: false
            }
          };
        }
      });
    }
  }

  analyzeChange() {
    this.guardError.next(false);
    console.log('analyze run...')
    this.allGpsPoints.forEach(wpt => {
      if (wpt.quizz.ask === (undefined || null) || (wpt.quizz.ask === "")) return this.guardError.next(false);
      if (wpt.quizz.answer1.answerBody !== (undefined && null) && (wpt.quizz.answer1.answerBody !== "")) {
        this.guardError.next(true);
      }
      if (wpt.quizz.answer2.answerBody !== (undefined && null) && (wpt.quizz.answer2.answerBody !== "")) {
        this.guardError.next(true);
      }
      if (wpt.quizz.answer1.answerBody !== (undefined && null) && (wpt.quizz.answer1.answerBody !== "")) {
        this.guardError.next(true);
      }
      if (wpt.quizz.answer1.value || wpt.quizz.answer2.value || wpt.quizz.answer3.value) return this.guardError.next(true);
      else return this.guardError.next(false);
    });
  }


  clickedMarker(infowindow) {
    if (this.previousInfoWindow) {
      this.previousInfoWindow.close();
    }
    this.previousInfoWindow = infowindow;
  }
  //Ferme la dernière fenetre info , permet de n'ouvrire qu'un seul info window a la fois...

  test() {
    this.persistenceData.tester();
  }

  focuser(WayPoint) {
    this.selectedWayPoint = this.allGpsPoints.indexOf(WayPoint);
    console.log(this.selectedWayPoint)
  }

  reverseOpenInfoWindow(WayPoint): boolean {
    //this.selectedWayPoint = this.allGpsPoints.indexOf(WayPoint);
    if (this.selectedWayPoint === this.allGpsPoints.indexOf(WayPoint)) {
      return true;
    }
  }

  //Focus couleur sur le WayPoint correspondant dans la list quand click sur le WayPoint dans Map
  save(activNav: boolean) {
    this.RALLY_API_GATEWAY.storeRally(this.persistenceData.previousSubmitDataValue, this.fullRally)
      .pipe(first())
      .subscribe(result => {
        console.log(result);
        if (activNav) {
          this.router.navigate(['dashboard']);
          this.alertService.success('Rally enregistré avec succès');
          return;
        }
      }, err => {
        if (activNav) {
          this.alertService.error('Probléme lors de l‘enregistrement du rally !');
          return;
        }
        console.log(err);
      });

  }
}
