import { Component, OnInit } from '@angular/core';
import { RallyCRUDrequestsService, AlertService, PersistenceFormsService } from '@/_services';
import { Router, ActivatedRoute, RouterState } from '@angular/router';
import { first } from 'rxjs/operators';
import { Rally } from '@/_models';
import { WayPoint} from '@/_models';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss']
})
export class DetailsComponent implements OnInit {
  id: string;
  viewDetails: boolean = true;
  rally: Rally;
  allGpsPoints: WayPoint[] = [];
  fullInfos: any;
  constructor(private RALLY_API_GATEWAY: RallyCRUDrequestsService, private router: Router, 
    private route: ActivatedRoute, private alertService: AlertService, private persistenceData: PersistenceFormsService) {
    this.id = this.route.snapshot.params['id'];
  }



  ngOnInit() {
    //this.persistenceData.passTokenStamper();
    this.persistenceData.tester();
    //Tout d'abord nous detruisons a chaque fois la persistence
    //Affin d'eviter l'accumulation de données sur les retour details... 

    this.persistenceData.destroyData();

    //Puis nous récuperons les données persistentes acquise en asynchrone via  readyForEdit()
    this.readyForEdit();
    this.rally = this.persistenceData.previousSubmitDataValue[0];
    this.allGpsPoints = this.persistenceData.previousSubmitDataValue[1];
    console.log(this.rally);
    console.log(this.allGpsPoints);
    console.log(this.fullInfos);
  }

  

  //Ce composant possède des composents enfants en sortie de routage..
  //Nous desactiverons la vue de ce composant lorsqu'un enfant sera chargé

  onActivate(event: any){
    this.viewDetails = false;
    /**+
     * this.persistenceData.destroyData();

    //Puis nous récuperons les données persistentes acquise en asynchrone via  readyForEdit()
    this.readyForEdit();
    this.rally = this.persistenceData.previousSubmitDataValue[0];
    this.allGpsPoints = this.persistenceData.previousSubmitDataValue[1];
    console.log(this.rally);
    console.log(this.allGpsPoints);
    console.log(this.fullInfos);
     */
  }

  onDesactivate(event: any){
    this.viewDetails = true;
  }

  //Suppression (soft delete) en base de donnée..

  clickDelete(){
    console.log('clicked');
    this.RALLY_API_GATEWAY.delete(this.id)
    .pipe(first())
    .subscribe(result => {
      this.router.navigate(['/dashboard']);
      this.alertService.success('Rally supprimé avec success');
    },
    err => {
      this.alertService.error('Impossible deffectuer la suppresion de e Rally !');
    });
  }

  //Récupération de TOUTES les données concernant le rally en base de donnée
  //L'Id de l'url nous servira de point de référence 
  //Nous effectuons cette action sur la page de details car nous avons besoin
  //De presque toutes les informations pour le recapitultif 
  //Nous conservons par la suite les données du rallys pour passer en mode edtition
  //Plutot que de r'envoyer une requete...

  readyForEdit(){
    this.RALLY_API_GATEWAY.getFullRally(this.id).pipe(first())
    .subscribe(result => {
      //Mise a jour du service de persistence
      //Ajout du rally obtennu dans cette requete asynchrone
      this.persistenceData.editMode(result);

      //Récupération des informations complémentaire au passage..
      //Contient des informations non présente dans les classes d'un rally (id en db, etc..);
      this.fullInfos = result['allInfos'];
      this.rally = this.persistenceData.previousSubmitDataValue[0];
      this.allGpsPoints = this.persistenceData.previousSubmitDataValue[1];
      console.log(this.fullInfos);
      console.log(this.rally);
      console.log(this.allGpsPoints);
    },
    err => {
      this.router.navigate(['/dashboard'])
      console.log(err);
    })

  }

  getRightAnswer(Waypoint){
    let right: String;
    this.allGpsPoints[this.allGpsPoints.indexOf(Waypoint)].quizz.answer1.value === true ? right = this.allGpsPoints[this.allGpsPoints.indexOf(Waypoint)].quizz.answer1.answerBody: null;
    this.allGpsPoints[this.allGpsPoints.indexOf(Waypoint)].quizz.answer2.value === true ? right = this.allGpsPoints[this.allGpsPoints.indexOf(Waypoint)].quizz.answer2.answerBody: null;
    this.allGpsPoints[this.allGpsPoints.indexOf(Waypoint)].quizz.answer3.value === true ? right = this.allGpsPoints[this.allGpsPoints.indexOf(Waypoint)].quizz.answer3.answerBody: null;
    return right;
  }

}
