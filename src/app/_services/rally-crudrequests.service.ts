import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { environment } from 'environments/environment';

@Injectable({
  providedIn: 'root'
})
export class RallyCRUDrequestsService {

  constructor(private http: HttpClient, private router: Router) { }

  creation(rally: any[], infos){
    return this.http.post<any>(environment.apiUrl + '/rally/creation', {rally, infos});
    //return this.http.post<any>('http://lisambert.akoatic.ovh:8010/rally/creation', {userToken, rally});
  }

  edition(id: string, rally: any[]){
    console.log('here');
    return this.http.put<any>(environment.apiUrl + '/rally/edit/' + id, {rally});
    //return this.http.put<any>('http://lisambert.akoatic.ovh:8010/rally/edit/' + id, {rally});
  }

  getRallys(){
    return this.http.get(environment.apiUrl + '/rally/getRallys');
    //return this.http.get('http://lisambert.akoatic.ovh:8010/rally/getRallys');
  }

  delete(id: string){
    return this.http.delete(environment.apiUrl + '/rally/delete/' + id);
    //return this.http.delete('http://lisambert.akoatic.ovh:8010/rally/delete/' + id);
  }
  getFullRally(id: string){
    return this.http.get(environment.apiUrl + '/rally/getFullRally/' + id);
    //return this.http.get('http://lisambert.akoatic.ovh:8010/rally/getFullRally/' + id);
  }

  storeRally(rally: any[], infos){
    return this.http.post<any>(environment.apiUrl + '/rally/storage', {rally, infos});
  }
  checkExistingRally(rally: any[]){
    return this.http.post<any>(environment.apiUrl + '/rally/checkExistingRally', {rally});
  }
}
