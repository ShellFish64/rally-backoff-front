import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';

import { UserInfos } from '@/_models/userInfos';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }
  
  register(user: UserInfos){
    //return this.http.post('http://lisambert.akoatic.ovh:8010/api/auth/signup', user);
    return this.http.post(environment.apiUrl + '/api/auth/signup', user);
  }

  getUser(): Observable<any>{
    console.log('toto')
    //return this.http.get('http://lisambert.akoatic.ovh:8010/api/auth/dashboard');
    return this.http.get(environment.apiUrl + '/api/auth/dashboard');
  }

  checkUser(): Observable<any>{
    return this.http.get(environment.apiUrl + '/api/auth/checkToken');
  }
}
