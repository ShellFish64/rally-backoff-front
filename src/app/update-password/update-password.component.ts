import { Component, OnInit } from '@angular/core';
import { ResetpwdService } from '@/_services/resetpwd.service';
import { Router, ActivatedRoute } from '@angular/router';
import { BehaviorSubject } from 'rxjs';

@Component({
  selector: 'app-update-password',
  templateUrl: './update-password.component.html',
  styleUrls: ['./update-password.component.scss']
})
export class UpdatePasswordComponent implements OnInit {
  password: string;
  passwordsafety: string;
  passwordLength = new BehaviorSubject<boolean>(undefined);
  repassword = new BehaviorSubject<boolean>(undefined);
  guardError = new BehaviorSubject<boolean>(false);
  token: string;


  constructor(private resetpwdService: ResetpwdService, private router: Router, private route: ActivatedRoute) { }
  
  ngOnInit() {
    this.route.params.subscribe( value => {
      console.log(value);
      this.token = value.token;
      console.log(this.token);
      this.resetpwdService.verifyTheResetTokenSignature(this.token);

    });
  }

  updatePass() {
    console.log('clicked');
    this.resetpwdService.updatePassword(this.token, this.passwordsafety);
  }

  verifyPass(){
    if(this.password !== undefined && this.password.length === 0){
      this.passwordLength.next(undefined);
      this.analyze();
      return;
    }
    if(!(this.password.length < 8)){
      this.passwordLength.next(true);
      this.analyze();
      return;
    } else {
      this.passwordLength.next(false);
      this.analyze();
      return;
    }
  }

  verifyRepass(){
    if(this.passwordsafety !== undefined && this.passwordsafety.length === 0){
      this.repassword.next(undefined);
      this.analyze();
      return;
    }
    if(this.passwordsafety === this.password){
      this.repassword.next(true);
      this.analyze();
      return;
    } else {
      this.repassword.next(false);
      this.analyze();
      return;
    }
    
  }
  analyze(){
    if (this.passwordLength.value && this.repassword.value) {
      this.guardError.next(true);
    } else {
      this.guardError.next(false);
    }
  }
}
