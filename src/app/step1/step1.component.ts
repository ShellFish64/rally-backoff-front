import { Component, OnInit, HostListener, ChangeDetectorRef } from '@angular/core';
import { Rally } from '@/_models/rally';
import { PersistenceFormsService, RallyCRUDrequestsService, AlertService } from '@/_services';
import { Router, ActivatedRoute } from '@angular/router';
import { first } from 'rxjs/operators';
import { BehaviorSubject } from 'rxjs';
import { NgbDate, NgbCalendar } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-step1',
  templateUrl: './step1.component.html',
  styleUrls: ['./step1.component.scss']
})
export class Step1Component implements OnInit {
 // name: any;
  // semblent inuties les 2 la ..expiration: any;
  rally: Rally;
  fullDatabaseInfosRally: any;

  /**
   * Flag de VERIFICICATION de TOUT LES INPUT GUARDS. 
   */
  verifyAllGuards = new  BehaviorSubject<boolean>(undefined);
  /**
   *Flag de VERIFICICATION de DISPONNIBLITÉ du NOM de RALLY.
   */
  rallyDisponibilty = new BehaviorSubject<boolean>(undefined);

  /**
   * ICI FLAGS PERMETTANT DE DEFINIR LE MODE D'UTILISATION ACTUEL.
   */
  creationPrepaMode = new BehaviorSubject<boolean>(undefined);
  editionPrepaMode = new BehaviorSubject<boolean>(undefined);
  editionPlayableMode = new BehaviorSubject<boolean>(undefined);

  /** 
    * GUARD GENERAL.
    * FLAG GENERAL DE VALIDATION EMPECHANT LE SWITCH STEPPING: GuardError.
    */
   guardError = new BehaviorSubject<boolean>(false);

  /**
   * ICI TOUT LES "INPUTS GUARDS" :
   * FLAGS d'INPUTS desquel DÉPENDENT la VALIDATION des champs et l'AFFICHAGE de MESSAGES.
   * Seul le GUARDERROR commence a FALSE pour passer a TRUE.
   * Les GAURDS de validation formulaire passent a TRUE si une ERREUR est détectée.
   */
  nameError = new BehaviorSubject<boolean>(undefined);
  dateError = new BehaviorSubject<boolean>(undefined);
  SpeCharErrorName = new BehaviorSubject<boolean>(undefined);
  keyPressCharErrorName = new BehaviorSubject<boolean>(undefined);
  keyPressCharErrorDate = new BehaviorSubject<boolean>(undefined);
  SpeCharErrorDate  = new BehaviorSubject<boolean>(undefined);

  //allInputsGuards = [this.rallyDisponibilty,this.nameError,this.dateError,this.SpeCharError,this.keyPressCharError,this.keyPressCharErrorDate];

  constructor(public persistenceData: PersistenceFormsService, private router: Router, private route: ActivatedRoute,
    private RALLY_API_GATEWAY: RallyCRUDrequestsService, private alertService: AlertService, private cdRef: ChangeDetectorRef, private calendarService: NgbCalendar) {
      console.log(this.editionPlayableMode.value);
      console.log(this.creationPrepaMode.value);
      console.log(this.editionPrepaMode.value);
  }

  /*@HostListener('window:beforeunload', ['$event'])
  beforeunloadHandler($event) {
    this.analyzeChange();
    if(this.guardError.value) this.save(false); 
    return false;
  }*/

  //NOUS PLACONS LES LYFE CYCLES HOOKS EN TETE DE FICHIER..

  ngOnInit() {
    console.log(this.editionPlayableMode.value);
    console.log(this.creationPrepaMode.value);
    console.log(this.editionPrepaMode.value);

    this.persistenceData.tester();
    console.log(this.guardError.value);
    this.rally = this.persistenceData.previousSubmitDataValue[0];
    this.fullDatabaseInfosRally = this.persistenceData.previousSubmitDataValue[2];
    console.log(this.fullDatabaseInfosRally);
    this.verifyRallyName();
    //this.checkExistingRally();
    this.verifyDate(); //Verification du nom uniquement en mode creation de prepa.. sinon edition du champs en DB automatique puisque hashlink dispo dans fullrallyInfos..
    this.unlocker();

    console.log(this.getTodayDate());
  }

  ngOnDestroy(){
    //this.unlocker();
    if(this.guardError.value) this.save(false);
    //this.creationPrepaMode.unsubscribe()
    //this.editionPrepaMode.unsubscribe();
    //this.editionPlayableMode.unsubscribe();   
  }

  ngDoCheck(){
    this.fullDatabaseInfosRally = this.persistenceData.previousSubmitDataValue[2];
    //console.log(this.fullDatabaseInfosRally);
    this.defineUsingMode(); 
    //console.log(this.editionPlayableMode.value);
    //console.log(this.creationPrepaMode.value);
    //console.log(this.editionPrepaMode.value);
  
  }

  /**
   * Définit le mode d'utilisation acutel: CRÉATION de PRÉPA, ÉDITION de PRÉPA, ÉDITION de PLAYABLE.
   * CRÉATION de PLAYABLE éffectué lorsque l'étape 6 est validé en mode CRÉATION/ÉDITION de PRÉPA 
   * Le MODE est définit selon 2 critères:
   * -La presence ou non d'informations provenenant de la Database concernant le rally(fullDatabaseInfosRally).
   * -La VALEUR/ÉTAT de la PRORIÉTÉ PLAYBLE dans fullDatabaseInfosRally.
   * ABSENCE de fullDatabaseInfosRally = MODE CRÉATION DE PRÉPA.
   * PRÉSENCE de fullDatabaseInfosRally, PROPRIÉTÉ 'PLAYABLE' UNDEFINED OU NULL = MODE ÉDITION DE PRÉPA.
   * PRÉSENCE de fullDatabaseInfosRally, PROPRIÉTÉ 'PLAYABLE' NON VIDE = MODE EDITION DE PLYABLE.
   */
   defineUsingMode(){
     if(this.fullDatabaseInfosRally && (this.fullDatabaseInfosRally['playable'] === null  || this.fullDatabaseInfosRally['playable'] === undefined)) {
       //console.log(this.fullDatabaseInfosRally);
        this.editionPrepaMode.next(true);
      return;
     } 
     if(this.fullDatabaseInfosRally === undefined){
      this.creationPrepaMode.next(true);
      //console.log(this.fullDatabaseInfosRally);
      return
     }
     if(this.fullDatabaseInfosRally && this.fullDatabaseInfosRally['playable'] !== undefined && this.fullDatabaseInfosRally['playable'] !== null){
        //console.log(this.fullDatabaseInfosRally);
        this.editionPlayableMode.next(true);
        this.creationPrepaMode.next(false); this.editionPrepaMode.next(false);
        return;
     }
   }

  /**
   * Fonction appelé a chaque changement d'état des INPUTS NAME et DATE
   * La valeur (true/false) du ErrorGuardFlag dépendra de la procédure et du résultat de cette fonction:
   * Verifie si les 2 champs correspondent parfaitement a nos attentes.
   * Champs name: ok + champs expiration === date + Verification de la disponibilité du nom du rally pour cet utilisateur (UNIQUEMENT en mode CREATION DE PREPA).
   * Alors: uniquement si les champs remplissent nos critères le guard reste/passe a true.
   */
   unlocker(){
     console.log('analyze..');
     console.log(this.dateError.value);
      console.log(this.SpeCharErrorDate.value, this.dateError.value, this.keyPressCharErrorDate.value, this.nameError.value, this.SpeCharErrorName.value, this.keyPressCharErrorName.value,  this.rallyDisponibilty.value)
    if(this.SpeCharErrorDate.value && this.dateError.value && this.keyPressCharErrorDate.value && this.nameError.value && 
      this.SpeCharErrorName.value && this.keyPressCharErrorName.value) {
      return this.guardError.next(true);
    }
    else return this.guardError.next(false);
   }

/**
   * Complément de vérification pour les saisies DIRECTS de CHARACTÉRES SPECIAUX pour le champ NAME
   * L'ETAT du GUARD keyPressCharError dépendra du resultat de cette fonction.
   * Et les MESSAGES d'erreur ou de confirmation de VALIDATION du CHAMP dépendrons de l'état du GUARD.
   * Nous utilisons 2 fonctions séparé pour l'input DATE et NAME pour ne pas avoir d'interférences.
  */
 verifyCharsOnPressName(){
  let regExName = new RegExp(/[^a-zA-Z0-9 ]/);
  console.log();
  if(!regExName.test(this.rally.name)){ console.log('bonne touche'); this.keyPressCharErrorName.next(true); return;}
  else console.log('mauvaise touche'); this.SpeCharErrorName.next(false); return this.keyPressCharErrorName.next(false);
}


 /**
   * Verification general de l'INPUT sur une EXPRESSION REGULIÉRE
   * L'ETAT du GUARD SpeCharError dépendra du resultat de cette fonction.
   * Et les MESSAGES d'erreur ou de confirmation de VALIDATION du CHAMP dépendrons de l'état du GUARD.
   * @param name 
  */ 
 verifyRallyName(){
   console.log('run vereifname')
    let name = this.rally.name;
    console.log(name);
    let regExName = new RegExp(/[^a-zA-Z0-9 ]/);
    console.log(this.SpeCharErrorName.value);
    console.log(regExName.test(this.rally.name));
    if(name !== undefined && name.length === 0){
      console.log('called')
      this.nameError.next(undefined);
      this.SpeCharErrorName.next(undefined);
      this.keyPressCharErrorName.next(undefined);
      return;
    }
    if(name.length >= 4 && name.length <= 40){
      this.nameError.next(true);
    } else {
      this.nameError.next(false);
    }
    if(!regExName.test(name)) { 
      this.SpeCharErrorName.next(true);
      this.keyPressCharErrorName.next(true);
      console.log(this.SpeCharErrorName.value);
    } else {
      this.SpeCharErrorName.next(false);
      console.log(this.SpeCharErrorName.value);
    }
    //this.checkExistingRally();
    //if(this.rally.name !== "" && this.checkExistingRally()) return this.rallyDisponibilty.next(true);
    //else return this.rallyDisponibilty.next(false);
 }
 
  /**
   * VERIFICATION de la VALIDITÉ de l'OBJET rally.expiration
   * @param datTocheck
  */
 verifyDate() {
  console.log('called')
  let regexDate = new RegExp(/^[0-9\.\-\/]+$/);
  if(this.rally.expiration === "" || this.rally.expiration !== undefined && this.rally.expiration === null){
    console.log('called')
    this.dateError.next(undefined);
    this.SpeCharErrorDate.next(undefined);
    this.keyPressCharErrorDate.next(undefined);
  }
   console.log(this.rally.expiration);
    let datTocheck = new Date(this.rally.expiration.month,this.rally.expiration.day,this.rally.expiration.year);
    console.log(datTocheck)
    if(datTocheck instanceof Date && !isNaN(datTocheck.getDate()) &&  !isNaN(datTocheck.getMonth()) && !isNaN(datTocheck.getDay())){
      this.dateError.next(true);
    } else {
      if(this.rally.expiration !== null && this.rally.expiration !== "") {
        this.dateError.next(false);
        console.log('here');
      }
    }
    if(regexDate.test(this.rally.expiration.day) && regexDate.test(this.rally.expiration.month) && regexDate.test(this.rally.expiration.year)) { 
      this.dateError.next(true);
      this.keyPressCharErrorDate.next(true);
      this.SpeCharErrorDate.next(true);
    } else {
      if(this.rally.expiration !== null && this.rally.expiration !== "") {
        this.dateError.next(false);
      }
      console.log(this.SpeCharErrorDate.value);
    }
    console.log(this.SpeCharErrorDate.value);
    console.log(this.keyPressCharErrorDate.value);
  }

  /**
   * Complément de vérification pour les saisies DIRECTS de CHARACTÉRES SPECIAUX pour le champ DATE
   * L'ETAT du GUARD keyPressCharError dépendra du resultat de cette fonction.
   * Et les MESSAGES d'erreur ou de confirmation de VALIDATION du CHAMP dépendrons de l'état du GUARD.
   * Nous utilisons 2 fonctions séparé pour l'input DATE et NAME pour ne pas avoir d'interférences.
  */
    verifyCharsOnPressDate(){
      //alert('verifypress')
      let regExDate = new RegExp(/^[0-9\.\-\/]+$/);
      if(this.rally.expiration !== null && this.rally.expiration.year !== undefined && this.rally.expiration.day !== undefined && this.rally.expiration.month !== undefined) return this.keyPressCharErrorDate.next(true);
      if(regExDate.test(this.rally.expiration)){ console.log('bonne touche'); this.keyPressCharErrorDate.next(true); return;}
      else console.log('mauvaise touche'); this.SpeCharErrorDate.next(false); return this.keyPressCharErrorDate.next(false);
    }

  /**
   * Appelé sur le DESTROY du composant, en l'occurence sur le NEXT-STEPPING du ROUTAGE(suivvnt,précédent).
   * HTTP request vers /storage/, transmet 2 PARAMÉTRES: Le JSON(stocké dans le servie de persistence) complet qui compose le rally , fullDatabaseInfosRally.
   * La differenciation d'une CRÉATION ou d'une ÉDITION se fait server-side. 
   * Suivant si fullDatabaseInfosRally est a UNDFINED/NULL ou NON VIDE.
   * @param activNav 
   */

   //
    save(activNav: boolean){
      this.RALLY_API_GATEWAY.storeRally(this.persistenceData.previousSubmitDataValue, this.fullDatabaseInfosRally)
      .pipe(first())
      .subscribe( result => {
        console.log(result);
        if(result !== undefined && result !== null && result.status !== undefined && result.status === 201){
          this.persistenceData.previousSubmitDataValue[2] = result.rallyCreated;
          this.fullDatabaseInfosRally = this.persistenceData.previousSubmitDataValue[2];
          this.alertService.successBlue('Un nouveau rally vient d\'etre ajouté à vos rallys connectés');
        }
        if(activNav){
          this.alertService.success('Rally enregistré avec succès');
          //plus tard nous gereros la redirection puis alert avec un token temporaire...
          return;
        }
      }, err => {
        console.log(err);
        if(err.status !== 201 && activNav){
          this.alertService.error('Probléme lors de l‘enregistrement du rally !');
          return;
        }
        if(err.status === 201){
          this.alertService.successBlue('Un nouveau rally vient d\'etre ajouté à vos rallys connectés');
          console.log(err);
        }
          console.log(err);
        })
      }

      getTodayDate(){
        return  this.calendarService.getToday();
      }

  }

