import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpHandler, HttpRequest, HttpEvent } from '@angular/common/http';
import { Observable, throwError, from } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { AuthenticationService } from '@/_services';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})

export class ErrorInterceptor implements HttpInterceptor {
    constructor(private authenticationService: AuthenticationService, private router: Router) {}
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(request).pipe(catchError(error => {
            /*if(error.status != 200 && error.status != 201){
                //this.authenticationService.logout();
                //location.reload(true);
                //this.router.navigate(['/dashboard']);
                //this.router.navigate(['/login']);
            }*/
            //this.router.navigate(['/login']);
            //request = request.clone();
            //return next.handle(request);
            //console.log(error);
            return throwError(error);
        }))
    }
    //Important to manage errors so beign not blocked
    //If currentUserTokentoken's is deleted from localStorage : reload to login page after 403 was catched (no blocking)
}