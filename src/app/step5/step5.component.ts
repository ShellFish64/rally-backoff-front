import { Component, OnInit, HostListener} from '@angular/core';
import { PersistenceFormsService, RallyCRUDrequestsService, AlertService } from '@/_services';
import { Router, ActivatedRoute } from '@angular/router';
import { WayPoint, Rally } from '@/_models';
import { first } from 'rxjs/operators';
import { environment } from 'environments/environment';

@Component({
  selector: 'app-step5',
  templateUrl: './step5.component.html',
  styleUrls: ['./step5.component.scss']
})
export class Step5Component implements OnInit {
  title: string = 'My first AGM project';
  lat: number;
  lng: number;
  rally: Rally;
  allGpsPoints: WayPoint[] = [];
  selectedWayPoint: number;
  fullRally: any;
  previousInfoWindow;
  file: File;
  url: any;
  env = environment;

  constructor(public persistenceData: PersistenceFormsService,private router: Router, 
    private RALLY_API_GATEWAY: RallyCRUDrequestsService, private alertService: AlertService, private route: ActivatedRoute) { 
  }

  @HostListener('window:beforeunload', ['$event'])
  beforeunloadHandler(event) {
    if(this.fullRally.playable !== null) this.save(false);
    if(this.fullRally.playable === null) this.save(true);
    return false;
  }

  clickedMarker(infowindow) {
    if (this.previousInfoWindow) {
        this.previousInfoWindow.close();
    }
    this.previousInfoWindow = infowindow;
  }
  //Ferme la dernière fenetre info , permet de n'ouvrire qu'un seul info window a la fois...

  ngOnInit() {
    this.rally = this.persistenceData.previousSubmitDataValue[0];
    this.lat = this.persistenceData.previousSubmitDataValue[0]['viewport']['lat'];
    this.lng = this.persistenceData.previousSubmitDataValue[0]['viewport']['lng'];
    console.log(this.rally.viewport)
  }

  onCenterChange(event: any){
    console.log(event);
    console.log(this.allGpsPoints);
    this.lat = event.lat,
    this.lng = event.lng 
    console.log(this.lat, this.lng);
  }

  ngOnDestroy(){
    if(this.fullRally !== undefined && this.fullRally.playable !== null){
      this.save(false);     
    }
    this.rally.viewport.lat = this.lat,
    this.rally.viewport.lng = this.lng  
  }

  ngDoCheck() {
    this.persistenceData.tester();
    this.allGpsPoints = this.persistenceData.previousSubmitDataValue[1];
    this.fullRally = this.persistenceData.previousSubmitDataValue[2];
  }
  

  setIconeSvgVsPng(WayPoint){
    if(this.allGpsPoints[this.allGpsPoints.indexOf(WayPoint)]['iconeSvg']['url'] !== null){
      return true;
    }
    if(this.allGpsPoints[this.allGpsPoints.indexOf(WayPoint)]['iconePng']['url'] !== null){
      return false;
    }
  }

  getIconeUrl(image): string{
    return this.env.apiUrl + '/static/upload/upload_ico/' + image;
  }

  getImageUrl(image): string{
    return this.env.apiUrl + '/static/upload/upload_img/' + image;
  }

  focuser(WayPoint) {
    this.selectedWayPoint = this.allGpsPoints.indexOf(WayPoint);
    console.log(this.selectedWayPoint)
  }

  reverseOpenInfoWindow(WayPoint): boolean {
    //this.selectedWayPoint = this.allGpsPoints.indexOf(WayPoint);
    if (this.selectedWayPoint === this.allGpsPoints.indexOf(WayPoint)) {
      return true;
    }
  }

  save(activNav: boolean){
    this.RALLY_API_GATEWAY.storeRally(this.persistenceData.previousSubmitDataValue, this.fullRally)
    .pipe(first())
    .subscribe( result => {
      console.log(result);
      if(activNav){
        this.router.navigate(['dashboard']);
        this.alertService.success('Rally enregistré avec succès');
        return;
      }
    }, err => {
      if(activNav){
        this.alertService.error('Probléme lors de l‘enregistrement du rally !');
        return;
      }
      console.log(err);
    });

  }

}
