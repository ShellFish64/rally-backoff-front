import { Answer } from './answers';

export class Quizz {
    quizz: {
        ask: String,
        answer1: Answer,
        answer2: Answer,
        answer3: Answer
    }
};
