//import { Quizz  } from './quizz';
export class WayPoint {
    coords: {
        lat: number;
        lng: number;   
    }
    quizz: {
        ask: String,
        answer1: {
        answerBody: String,
        value: Boolean
        },
        answer2: {
        answerBody: String,
        value: Boolean
        },
        answer3: {
        answerBody: String,
        value: Boolean
        }
    }
    image: {
        url: string;
    }
    iconePng: {
        url: string;
    }
    iconeSvg: {
        url: string;
    }

    public constructor (param: WayPoint){
        console.log(this.coords)
        this.coords = {
            lat: param.coords.lat,
            lng: param.coords.lng
        }
        this.quizz = param.quizz
        this.image = param.image
        this.iconePng = param.iconePng
        this.iconeSvg = param.iconeSvg
    }
    /*constructor(lat, lng, ask, answer1, answer2, answer3, value1, value2, value3, urlImg, urlSvg, urlPng){
        this.coords = {},
        this.coords.lat = lat,
        this.coords.lng = lng,
        this.quizz.ask = ask,
        this.quizz.answer1.answerBody = answer1,
        this.quizz.answer1.value = value1,
        this.quizz.answer2.answerBody = answer2,
        this.quizz.answer2.value = value2,
        this.quizz.answer3.answerBody = answer3,
        this.quizz.answer3.value = value3,
        urlImg === 'photo' ? this.image.url = urlImg: null;
        urlSvg === 'iconeSvg' ? this.iconeSvg.url = urlSvg: null;
        urlPng === 'iconePng' ? this.iconePng.url = urlPng: null;
    }*/
}