import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthenticationService } from '@/_services/authentication.service';
import { Router, ActivatedRoute } from '@angular/router';

import { first } from 'rxjs/operators'
import { AlertService } from '@/_services';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private authenticationService: AuthenticationService,
    private alertService: AlertService
  ) { 
    //Le reste sera verifié sur le dash (signature coté back-end)
      console.log('login called !!');
      if(this.authenticationService.currentUserToken){
        console.log('already connexted') 
        this.router.navigate(['/dashboard']);
      } 
  }
  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      mail: ['', Validators.required],
      password: ['', Validators.required]
    })

    //get reutn url from route parameters or default to '/'
    //this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }

  get f() { return this.loginForm.controls; }

  onSubmit() {
    this.submitted = true;

    if(this.loginForm.invalid){
      return;
    }

    this.loading = true;
    this.authenticationService.login(this.f.mail.value, this.f.password.value)
      .pipe(first())
      .subscribe(
        data => {
          this.router.navigate(['/dashboard']);
        },
        error => {
          this.loading = false;
          console.log(error)
          this.alertService.error('Identifiants incorrect, verrifiez vos informations !');// ou error.error pour donner plus d'informations...
        }
      )

  }

}
