import { Component, OnInit } from '@angular/core';
import { ResetpwdService } from '@/_services/resetpwd.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-resetpwd',
  templateUrl: './resetpwd.component.html',
  styleUrls: ['./resetpwd.component.scss']
})
export class ResetpwdComponent implements OnInit {

  constructor(private resetpwdService: ResetpwdService, private router: Router) { }
  userMail: string;

  ngOnInit() {

  }

  sendToken(userMail: string){
    console.log(this.userMail);
    this.resetpwdService.sendToken(this.userMail);
  }

  verifyToken(){

  }

}
