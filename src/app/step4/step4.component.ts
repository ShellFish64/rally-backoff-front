import { Component, OnInit, HostListener } from '@angular/core';
import { PersistenceFormsService, AlertService, RallyCRUDrequestsService,  } from '@/_services';
import { Router, ActivatedRoute } from '@angular/router';
import { WayPoint, Rally } from '@/_models';
import { HttpClient } from '@angular/common/http';
import { first } from 'rxjs/operators';
import { Ng2ImgMaxService } from 'ng2-img-max';
import { environment } from 'environments/environment';

@Component({
  selector: 'app-step4',
  templateUrl: './step4.component.html',
  styleUrls: ['./step4.component.scss']
})
export class Step4Component implements OnInit {
  title: string = 'My first AGM project';
  rally: Rally;
  latitude: number;
  longitude: number;
  allGpsPoints: WayPoint[] = [];
  selectedWayPoint: number;
  previousInfoWindow;
  file: File;
  fullRally: any;
  icone: any;
  env = environment
  constructor(private ng2ImgMaxService: Ng2ImgMaxService,public persistenceData: PersistenceFormsService,
    private router: Router, private http: HttpClient, private alertService: AlertService, 
    private RALLY_API_GATEWAY: RallyCRUDrequestsService, private route: ActivatedRoute) { 
    
  }

  @HostListener('window:beforeunload', ['$event'])
  beforeunloadHandler(event) {
    if(this.fullRally.playable !== null) this.save(false);
    if(this.fullRally.playable === null) this.save(true);
    return false;
  }

  clickedMarker(infowindow) {
    if (this.previousInfoWindow) {
        this.previousInfoWindow.close();
    }
    this.previousInfoWindow = infowindow;
  }
  //Ferme la dernière fenetre info , permet de n'ouvrire qu'un seul info window a la fois...
  
  ngOnInit() {
    //this.persistenceData.tester();
    //this.allGpsPoints = this.persistenceData.previousSubmitDataValue[1];
    //console.log(this.allGpsPoints);
    this.latitude = this.persistenceData.previousSubmitDataValue[0]['viewport']['lat'];
    this.longitude = this.persistenceData.previousSubmitDataValue[0]['viewport']['lng'];
    this.rally = this.persistenceData.previousSubmitDataValue[0];
  }
  //Préparation des Objets et propriétés,pret a recevoir les Url medias.

  ngDoCheck() {
    this.persistenceData.tester();
    //this.rally = this.persistenceData.previousSubmitDataValue[0];
    this.fullRally = this.persistenceData.previousSubmitDataValue[2];
    this.allGpsPoints = this.persistenceData.previousSubmitDataValue[1];
    this.allGpsPoints.forEach(wpt => {
      wpt.image === undefined ? wpt.image = {url: null} : null;
      wpt.image === null ? wpt.image = {url: null} : null;
      wpt.iconePng === undefined ? wpt.iconePng = {url: null}: null;
      wpt.iconePng === null ? wpt.iconePng = {url: null}: null;
      wpt.iconeSvg === undefined ? wpt.iconeSvg = {url: null}: null;
      wpt.iconeSvg === null ? wpt.iconeSvg = {url: null}: null;
    });
    if(this.route.snapshot.url[0]['path'] === 'edit-step'){
      //this.latitude = this.persistenceData.previousSubmitDataValue[0]['viewport']['lat'];
      //this.longitude = this.persistenceData.previousSubmitDataValue[0]['viewport']['lng'];
      //this.rally = this.persistenceData.previousSubmitDataValue[0];
    }
    console.log(this);
    console.log(this.longitude, this.latitude);
  }
  //Préparation des Objets et propriétés,pret a recevoir les Url medias.


  ngOnDestroy(){
    if(this.fullRally !== undefined && this.fullRally.playable !== null){
      this.save(false);
    }
    console.log(this.latitude, this.longitude); 
    this.rally.viewport.lat = this.latitude;
    this.rally.viewport.lng = this.longitude;
    console.log(this.rally.viewport);
  }

  onCenterChange(event: any){
    console.log(event);
    console.log(this.allGpsPoints);
    this.latitude = event.lat,
    this.longitude = event.lng 
    console.log(this.latitude, this.longitude);
  }

  onSelectedFileImage(event: any, WayPoint){
    if (event.target.files && event.target.files[0]) {
        console.log(event.target.files);

        //Resize l'image(async) puis suite des instruction asynchrones
        //2 niveaux de call back pour assurer la prevalence du resizing avant TOUT.
        this.ng2ImgMaxService.resize([event.target.files[0]], 400, 600).subscribe((result)=>{

        let reader = new FileReader();
        //Reception des dataURL pour upload
        reader.onload = (event: ProgressEvent) => {
          const file = (<FileReader>event.target).result;
          //Upload
          this.upload_img(file).pipe(first())
          .subscribe(file => {
            this.allGpsPoints[this.allGpsPoints.indexOf(WayPoint)].image.url = file.media;
            console.log(this.allGpsPoints[this.allGpsPoints.indexOf(WayPoint)].image.url);
          }, err => {
            this.alertService.error('Error au moment de lupload');
          })
        }

        //Quand File recu this.url prend son DataURL(async)
        reader.readAsDataURL(result);
      })
  }
}
  onSelectedFileIcone(event: any, WayPoint){
    if (event.target.files && event.target.files[0]) {
      console.log(event.target.files);

      //Si le type du fichier est un png nous ajoutons l'url a la propriété iconePng du waypoint.

      if (event.target.files[0].type === 'image/png') {

        //Resize l'image(async) puis suite des instruction asynchrones
        //2 niveaux de call back pour assurer la prevalence du resizing avant TOUT.
        this.ng2ImgMaxService.resize([event.target.files[0]], 70, 60).subscribe((result)=>{

          let reader = new FileReader();
          //Reception des dataURL pour upload
          reader.onload = (event: ProgressEvent) => {
            console.log(result);
            const file = (<FileReader>event.target).result;
                     
            //Upload
            this.upload_png(file).pipe(first())
            .subscribe(file => {
              this.allGpsPoints[this.allGpsPoints.indexOf(WayPoint)].iconePng.url = file.media;
              this.allGpsPoints[this.allGpsPoints.indexOf(WayPoint)].iconeSvg.url = null;
              console.log(this.allGpsPoints[this.allGpsPoints.indexOf(WayPoint)].image.url);
            }, err => {
              this.alertService.error('Error au moment de lupload');
            });
          }
          //Quand File recu this.url prend son DataURL(async)
          reader.readAsDataURL(result);
        });
      }

      //Si le type du fichier est un svg nous ajoutons l'url a la propriété iconeSvg du waypoint.

      if (event.target.files[0].type === 'image/svg' || event.target.files[0].type === 'image/svg+xml') {

        let reader = new FileReader();

        //Reception des dataURL pour upload
        reader.onload = (event: ProgressEvent) => {
          console.log((<FileReader>event.target));
          const file = (<FileReader>event.target).result;

          //upload
          this.upload_svg(file).pipe(first())
            .subscribe(file => {
              this.allGpsPoints[this.allGpsPoints.indexOf(WayPoint)].iconeSvg.url = file.media;
              this.allGpsPoints[this.allGpsPoints.indexOf(WayPoint)].iconePng.url = null;
            }, err => {
              this.alertService.error('Error au moment de lupload');
            });
        }
        //Quand File recu this.url prend son DataURL(async)
        reader.readAsDataURL(event.target.files[0]);
      }
    }
  }
  //Icone Uploader: 2 cas possible nous recevons png,svg,svg+xml pas de .ico pour le moment.
  //Selon que nous recevons un type png/svg, l'objet recois l'url dans le bon Objet.

  upload_img(image){
    return this.http.post<any>(environment.apiUrl + '/rally/upload', {image});
  }
  upload_png(iconePng){
    return this.http.post<any>(environment.apiUrl + '/rally/upload', {iconePng});
}
  upload_svg(iconeSvg){
    return this.http.post<any>(environment.apiUrl + '/rally/upload', {iconeSvg});
  }

  getIconeUrl(image): string{
    return this.env.apiUrl + '/static/upload/upload_ico/' + image;
  }

  getImageUrl(image): string{
    return this.env.apiUrl + '/static/upload/upload_img/' + image;
  }

  focuser(WayPoint) {
    this.selectedWayPoint = this.allGpsPoints.indexOf(WayPoint);
    console.log(this.selectedWayPoint)
  }

  reverseOpenInfoWindow(WayPoint): boolean {
    //this.selectedWayPoint = this.allGpsPoints.indexOf(WayPoint);
    if (this.selectedWayPoint === this.allGpsPoints.indexOf(WayPoint)) {
      return true;
    }
  }

  save(activNav: boolean){
    this.RALLY_API_GATEWAY.storeRally(this.persistenceData.previousSubmitDataValue, this.fullRally)
    .pipe(first())
    .subscribe( result => {
      console.log(result);
      if(activNav){
        this.router.navigate(['dashboard']);
        this.alertService.success('Rally enregistré avec succès');
        return;
      }
    }, err => {
      if(activNav){
        this.alertService.error('Probléme lors de l‘enregistrement du rally !');
        return;
      }
      console.log(err);
    });

  }

}
