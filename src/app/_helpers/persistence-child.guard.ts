import { Injectable } from '@angular/core';
import { Router, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, CanActivateChild } from '@angular/router';
import { Observable } from 'rxjs';
import { PersistenceFormsService, UserService } from '@/_services';
import { map } from 'rxjs/operators';
import { WayPoint, Rally } from '@/_models';

@Injectable({
  providedIn: 'root'
})
export class PersistenceChildGuard implements  CanActivateChild{
  waypoint: WayPoint[];
  rally: Rally;
  constructor(
    private router: Router,
    private userService: UserService,
    private peristenceData: PersistenceFormsService
  ){console.log('guard called')}

  canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return this.userService.getUser().pipe(
      map(
      (user, error) => {
        if(user) {
          this.waypoint = this.peristenceData.previousSubmitDataValue[1];
          if(this.waypoint.length > 0){
            console.log('there is wpts');
            return true;
          } else {
            this.router.navigate(['/dashboard'])
            return false;
          }
        } 
        if(error) {
          this.router.navigate(['/login'])
        }
        return false;
      }));
  }
}
