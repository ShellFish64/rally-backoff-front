import { RallyDbData } from '@/_models';

export class Rally{
    name: any;
    expiration: any;
    teams: Array<{name: string, color: string, mail: string}>;
    viewport: {
        lat: number,
        lng: number,
    }
    zoom: number
}
