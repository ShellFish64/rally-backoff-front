import { Injectable } from '@angular/core';
import { Rally } from '@/_models/rally';
import { WayPoint } from '@/_models/waypoint';


@Injectable({
  providedIn: 'root'
})

export class PersistenceFormsService {
//Tout ce qui compose un rally est ici...
//Son nom, sa date d'expiration ces equipes et ces waypoints..

  rally: Rally = {
    name: '',
    expiration: '',
    teams: null,
    viewport: {
      lat:  48.856614,
      lng: 2.3522219,
    },
    zoom: 5
  };

  allGpsPoints: WayPoint[] = [];
  fullRally: any;

  private previousSubmitData: Array<any> = [this.rally, this.allGpsPoints]; 
  //Nous stockerons TOUTES nos données persistentes dans ce tableau
  //Plus tard nous tenterons d'implémenter un Observable.

  //Nous nous servirons de ce "passerport" pour bloquer l'accès a l'étape1
  //De cette manière ne seront autorisé que les chargement du composant depuis le dashboard
  private passToken: boolean = true;
  //subject = new BehaviorSubject(false);

  preventDestroy:boolean = false;


  constructor() { 
  }

  submitData(submitData){
    this.previousSubmitData.push(submitData);
  }

  editMode(fullRally){
    console.log(fullRally);
    this.rally.name = fullRally['rally']['name'];
    this.rally.expiration = {
      day: fullRally['rally']['expiration']['day'],
      month: fullRally['rally']['expiration']['month'],
      year: fullRally['rally']['expiration']['year']
    }
    this.rally.teams = fullRally['rally']['teams'];
    this.allGpsPoints = fullRally['waypoints'];
    this.fullRally = fullRally['allInfos'];
    this.previousSubmitData = [this.rally, this.allGpsPoints, this.fullRally];
  }
  //Le but de cette fonction est de mettre en persistence les donnée recu en asynchrone qui lui sont passé en paramétres.
  //Nous ne stockons pas ici les fonctions dont elle dépend puisque cette classe ne concerne que la partie persistence de notre appli.
  //Inférence de l'argument ici car nous récupérons la totalité des données SQL concernant un rally dont certaines figurent pas dans nos classes(id,updated at etc).

  
  public get previousSubmitDataValue(){
    return this.previousSubmitData;
  }

  public get getToken(){
    return this.passToken;
  }

  public get getPreventDestroy(){
    return this.preventDestroy;
  }
  
  destroyData(){
    this.rally = {
      name: '',
      expiration: '',
      teams: null,
      viewport: {
        lat: 48.856614,
        lng:  2.3522219
      },
      zoom: 1
    };
    this.allGpsPoints = [];
    this.fullRally = '';
    this.previousSubmitData = [this.rally, this.allGpsPoints];
    //this.passToken = false;
  }
  //Nous appelerons cette fonction dans le composant dashboard pour detruire a chaque chargement du composant la persistence avant de la reconsituer

  passTokenStamper(){
    this.passToken = true;
  }
  passTokenUnStamper(){
    this.passToken = false;
  }

  activPreventDestroy(){
    this.preventDestroy = true;
  }
  deactivPreventDestroy(){
    this.preventDestroy = false;
  }

  tester() {
    console.log(this.previousSubmitData);
    console.log(this.getToken);
  }

}
