import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, Router, CanActivate } from '@angular/router';
import { AuthenticationService, UserService } from '@/_services';
import { map, catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { HttpErrorResponse } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(
    private router: Router,
    private userService: UserService
  ) { console.log('guard called') }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return this.userService.getUser().pipe(
      map(
        (user, error) => {
          //const currentUser = this.autthenticationService.currentUserToken;
          if (user) {
            console.log('recu');
            return true;
          }
          if (error) {
            console.log(error);
          }
          return false;
        }), catchError((error: HttpErrorResponse) => {
          if (error.error instanceof ErrorEvent) {
            // A client-side or network error occurred. Handle it accordingly.
            console.error('An error occurred:', error.error.message);
          } else {
            // The backend returned an unsuccessful response code.
            // The response body may contain clues as to what went wrong,
            console.error(
              `Backend returned code ${error.status}, ` +
              `body was: ${error.error}`);
          }
          // return an observable with a user-facing error message
          console.log(this.router);
          this.router.navigate(['/login']);
          return throwError(
            'Something bad happened; please try again later.');
        }));
  }
  // Asynchrone token checking
  // If the currentUser into LocalLocalStorage has no valid token
  // Intercepted and redirected on login page
  // Intercept it not on component checking but BEFORE :)


}
