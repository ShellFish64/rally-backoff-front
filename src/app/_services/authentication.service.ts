import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from '../../environments/environment';

import { UserToken } from '@/_models/';


@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  private currentUserSubject: BehaviorSubject<UserToken>;
  public currentUser: Observable<UserToken>;

  constructor(private http: HttpClient) { 
    this.currentUserSubject = new BehaviorSubject<UserToken>(JSON.parse(localStorage.getItem('userToken')));
    this.currentUser = this.currentUserSubject.asObservable();
    //Hot/Multicast Observable
    //On cache l'identité de l'observable source (this.currentSubject) aux components beneficiaires de l'injection  
    //Sécurité de manière a ne pas pouvoire emmetter de nouvelles valeur avec bhsbj.next(param) ailleurs que depuis ce service
    //BehaviorSubject herite Subject qui a .next(param) pour emettre de nouvelles valeurs
  }

  public get currentUserToken(): UserToken{
    return this.currentUserSubject.value;
  }

  login(mail, password) {
    console.log(password);
    console.log(mail);
    //return this.http.post<any>('http://lisambert.akoatic.ovh:8010/api/auth/signin', { mail , password })
    return this.http.post<any>(environment.apiUrl + '/api/auth/signin', { mail , password })
      .pipe(map(user => {
        localStorage.setItem('userToken', JSON.stringify(user));
        this.currentUserSubject.next(user);
        //Si identifients de l'utilisateur : ok, 
        //la nouvelle value du multicast observable = l'utilisateur
        return user;
      }))
  }
  
  logout(){
    localStorage.removeItem('userToken');
    this.currentUserSubject.next(null);
  }
}
