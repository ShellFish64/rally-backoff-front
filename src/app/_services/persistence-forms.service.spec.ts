import { TestBed } from '@angular/core/testing';

import { PersistenceFormsService } from './persistence-forms.service';

describe('PersistenceFormsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PersistenceFormsService = TestBed.get(PersistenceFormsService);
    expect(service).toBeTruthy();
  });
});
