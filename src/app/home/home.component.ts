import { Component, OnInit } from '@angular/core';
import { AuthenticationService, PersistenceFormsService } from '@/_services';
import { RallyCRUDrequestsService } from '@/_services/rally-crudrequests.service';
import { first } from 'rxjs/operators';
import { RallyDbData } from '@/_models';

import {UserToken} from '@/_models'
import { Router } from '@angular/router';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  currentUser: UserToken;
  /*rallys: RallyDbData;*/
  rallys: any;
  public name: string;
  constructor(private authenticationService: AuthenticationService,  private RALLY_API_GATEWAY: RallyCRUDrequestsService, 
    private persistenceData: PersistenceFormsService, private router: Router) {
      let token = this.authenticationService.currentUserToken.accessToken;
      let decoded = this.parseJwt(token);
      this.name = decoded.name; 
  }
  ngOnInit() {
    this.persistenceData.destroyData();
    this.persistenceData.passTokenStamper();
    this.RALLY_API_GATEWAY.getRallys().pipe(first())
    .subscribe(
        rallys => {
          this.rallys = rallys
          console.log(this.rallys);
        }
    );
    this.persistenceData.passTokenStamper();
    this.persistenceData.tester();
    console.log(this.persistenceData.getPreventDestroy);
    this.persistenceData.deactivPreventDestroy();
    console.log(this.persistenceData.getPreventDestroy);
  }

  parseJwt(token) {
    var base64Url = token.split('.')[1];
    var base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
    var jsonPayload = decodeURIComponent(atob(base64).split('').map(function(c) {
        return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
    }).join(''));

    return JSON.parse(jsonPayload);
};
}
