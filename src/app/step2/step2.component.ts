import { Component, OnInit, HostListener } from '@angular/core';
import { PersistenceFormsService, RallyCRUDrequestsService, AlertService } from '@/_services';
import { Router, ActivatedRoute } from '@angular/router';
import { WayPoint } from '@/_models/waypoint';
import { first } from 'rxjs/operators';
import { BehaviorSubject } from 'rxjs';
import { Rally } from '@/_models';

@Component({
  selector: 'app-step2',
  templateUrl: './step2.component.html',
  styleUrls: ['./step2.component.scss']
})
export class Step2Component implements OnInit {
  title: string = 'My first AGM project';
  lat: number;
  lng: number;
  allGpsPoints: WayPoint[] = [];
  rally: Rally;
  fullRally: any;
  selectedWayPoint: number;
  guardError = new BehaviorSubject<boolean>(false);

  constructor(private persistenceData: PersistenceFormsService, private router: Router,
    private RALLY_API_GATEWAY: RallyCRUDrequestsService, private alertService: AlertService, private route: ActivatedRoute) {
    this.rally = this.persistenceData.previousSubmitDataValue[0];
    this.allGpsPoints = this.persistenceData.previousSubmitDataValue[1];
    console.log(this.allGpsPoints);
    //Si mode édition: récupération model ET vue des données..(expérience utilisateur plus agréable)..
  }
  /*@HostListener('window:beforeunload', ['$event'])
  beforeunloadHandler(event) {
    if(this.fullRally.playable !== null) this.save(false);
    if(this.fullRally.playable === null) this.save(true);
    return false;
  }*/
  WayPointAddor($event) {
    this.allGpsPoints.push($event);
    console.log('WayPoint : ', this.allGpsPoints);
  }

  ngDoCheck() {
    this.rally = this.persistenceData.previousSubmitDataValue[0];
    this.allGpsPoints = this.persistenceData.previousSubmitDataValue[1];
    this.fullRally = this.persistenceData.previousSubmitDataValue[2];
    console.log(this.fullRally);
    this.analyzeChange()
  }
  //Step suivant

  ngOnInit() {
    this.lat = this.persistenceData.previousSubmitDataValue[0]['viewport']['lat'];
    this.lng = this.persistenceData.previousSubmitDataValue[0]['viewport']['lng'];
    this.rally = this.persistenceData.previousSubmitDataValue[0];
  }
  //Simple test
  test($event) {
    if ($event <= 1) {
      this.rally.zoom = 1;
    }
    console.log($event);
    console.log(this.rally.zoom);
  }

  onCenterChange($event) {
    console.log($event);
    this.lat = $event.lat,
    this.lng = $event.lng
    console.log(this.persistenceData.previousSubmitDataValue[0]['viewport']);
  }

  ngOnDestroy() {
    if (this.fullRally !== undefined && this.fullRally.playable !== null && this.fullRally.playable !== undefined) {
      //alert('save step2 destroy');
      this.save(false);
    }
    this.rally.viewport.lat = this.lat,
    this.rally.viewport.lng = this.lng
  }
  //Cette fonction sur le Destroy n'est destiné qu'au mode edit-step
  //le mode creation-step dispose d'un composant parent qui, au Destroy, execute le save..

  analyzeChange() {
    if (this.allGpsPoints.length > 0) {
      this.guardError.next(true);
      return
    } else {
      this.guardError.next(false);
    }
  }


  getDraggingPosition($event, WayPoint) {
    this.allGpsPoints[this.allGpsPoints.indexOf(WayPoint)]['coords']['lat'] = $event.coords.lat;
    this.allGpsPoints[this.allGpsPoints.indexOf(WayPoint)]['coords']['lng'] = $event.coords.lng;
  }
  //Changement en direct des valeur decimal dans l'input

  removeWayPoint($event) {
    this.allGpsPoints.splice(this.allGpsPoints.indexOf($event), 1);
  }
  //Suppresion d'un WayPoint


  focuser(WayPoint) {
    this.selectedWayPoint = this.allGpsPoints.indexOf(WayPoint);
    console.log(this.selectedWayPoint)
  }

  reverseOpenInfoWindow(WayPoint): boolean {
    //this.selectedWayPoint = this.allGpsPoints.indexOf(WayPoint);
    if (this.selectedWayPoint === this.allGpsPoints.indexOf(WayPoint)) {
      return true;
    }
  }

  save(activNav: boolean) {
    this.RALLY_API_GATEWAY.storeRally(this.persistenceData.previousSubmitDataValue, this.fullRally)
      .pipe(first())
      .subscribe(result => {
        console.log(result);
        if (activNav) {
          this.router.navigate(['/dashboard']);
          this.alertService.success('Rally enregistré avec succès');
          return;
        }
      }, err => {
        if (activNav) {
          this.alertService.error('Probléme lors de l‘enregistrement du rally !');
          return;
        }
        console.log(err);
      });

  }
}
