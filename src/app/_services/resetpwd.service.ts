import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import { AlertService } from '@/_services';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class ResetpwdService {

  constructor(private http: HttpClient, private alertService: AlertService, private router: Router) {
  }
  
  sendToken(mail: string) {
    return this.http.post<any>(environment.apiUrl + '/api/auth/pwd_reset', { mail })
    .subscribe(user => { 
      this.alertService.success('Un e-mail de validation vient de vous etre envoyé! Pensez a consulter vos indésirables.');
    },
    err => {
      console.log(err);
      this.alertService.error('Erreur: votre addresse e-mail semble inconnue')
    });
  }

  verifyTheResetTokenSignature(token: string){ 
    return this.http.get(environment.apiUrl + '/api/auth/reset/' + token )
    .subscribe(token => { 
      console.log(token);
    },
    err => {
      console.log(err);
      this.router.navigate(['/login']);
      this.alertService.error('Erreur: ce jeton de modification semble invalide ou dépassé');
    });

  }

  updatePassword(token, password) {
    return this.http.post(environment.apiUrl + '/api/auth/reset/' + token, {token, password} )
    .subscribe(success => {
      this.router.navigate(['/login']);
      this.alertService.success('Mot de passe modifié avec succès!');
    }, err => {
      if(err.message) {
        this.alertService.error(err.message);
      }
      this.alertService.error('Erreur au moment du processus de modification du mot de passe...')
    })
  }
  
}
