import { TestBed, async, inject } from '@angular/core/testing';

import { PersistenceChildGuard } from './persistence-child.guard';

describe('PersistenceChildGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PersistenceChildGuard]
    });
  });

  it('should ...', inject([PersistenceChildGuard], (guard: PersistenceChildGuard) => {
    expect(guard).toBeTruthy();
  }));
});
