import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { PersistenceFormsService } from './persistence-forms.service';

@Injectable({
  providedIn: 'root'
})
export class NavigationStepService {

  constructor(private router: Router, private persistenceData: PersistenceFormsService) { }

  

  previous(path){
    //this.persistenceData.activeEditMode();
    //this.router.navigate([path]);
  }
  onSubmit(dataToPush, path){
    //this.persistenceData.submitData(dataToPush); // ajouter un suscribe pour valider le routeage sur nextstep
    //console.log("ok");
    //this.router.navigate([path]);
    //Ajout des données du formulaire dans la persistence puis routage vers étape suivante.
  }
}


