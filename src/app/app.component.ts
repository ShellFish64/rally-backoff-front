import { Component } from '@angular/core';
import { UserToken } from './_models';
import { AuthenticationService } from './_services';
import { UserService } from '@/_services';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styles: ['svg { height: 35px; }','a {cursor: pointer; }'],
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  currentUser: UserToken;
  validUser: boolean = false;
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private authenticationService: AuthenticationService,
    private userService: UserService,
  ){
    //Rajouter un controle de la signature du token
    this.authenticationService.currentUser.subscribe(x => {
      console.log(x);
      if (x !== null && x !== undefined) { 
        this.currentUser = x;
        this.userService.checkUser()
        .subscribe(validUser => {
          this.validUser = true;
          console.log(this.validUser);
        },err => {
          this.authenticationService.logout();
        })
      } else {
        this.validUser = false;
        //this.router.navigate(['/login']);
      }
    });
  }
  ngOnInit() {

  }
  logout() {
    this.authenticationService.logout();
    this.router.navigate(['/login']);
  }
}
