import { TestBed } from '@angular/core/testing';

import { RallyCRUDrequestsService } from './rally-crudrequests.service';

describe('RallyCRUDrequestsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RallyCRUDrequestsService = TestBed.get(RallyCRUDrequestsService);
    expect(service).toBeTruthy();
  });
});
