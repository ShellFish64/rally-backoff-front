import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, CanActivate, Router } from '@angular/router';
import { Observable, BehaviorSubject } from 'rxjs';
import { AuthenticationService, UserService } from '@/_services';
import { map, catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { HttpErrorResponse } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class NoAuthGuard implements  CanActivate{
  constructor(private authenticationService: AuthenticationService, private userService: UserService, private router: Router) {
  }
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {

    return this.userService.checkUser().pipe(
          map(validUser => {
            this.router.navigate(['/dashboard']);
            return false;
          },err => {
            return true;
          }), catchError((error: HttpErrorResponse) => {
              return new BehaviorSubject(true);
          }));
  }
  
}
