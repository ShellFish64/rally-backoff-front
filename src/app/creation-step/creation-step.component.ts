import { Component, OnInit, HostListener} from '@angular/core';
import { PersistenceFormsService, RallyCRUDrequestsService, AlertService } from '@/_services';
import { Router, ActivatedRoute} from '@angular/router';
import { WayPoint } from '@/_models';
import { Rally } from '@/_models';
import { first } from 'rxjs/operators';
import { BehaviorSubject } from 'rxjs';


@Component({
  selector: 'app-creation-step',
  templateUrl: './creation-step.component.html',
  styleUrls: ['./creation-step.component.scss']
})
export class CreationStepComponent implements OnInit {
  fullDatabaseInfosRally: any;
  rally: Rally;
  guardError = new BehaviorSubject<boolean>(false);
  constructor(public persistenceData: PersistenceFormsService,private router: Router,
    private RALLY_API_GATEWAY: RallyCRUDrequestsService,
    private alertService: AlertService, private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.rally = this.persistenceData.previousSubmitDataValue[0];
    this.fullDatabaseInfosRally = this.persistenceData.previousSubmitDataValue[2];
  }


  ngOnDestroy(){
    this.fullDatabaseInfosRally = this.persistenceData.previousSubmitDataValue[2];
    //this.analyzeChange();
    console.log(this.guardError.value);
    //if(this.guardError.value);
    //alert('save from creation-step');
   if (!this.persistenceData.getPreventDestroy) this.save(false);  this.persistenceData.deactivPreventDestroy; 
  }
  
  save(activNav: boolean){
    this.RALLY_API_GATEWAY.storeRally(this.persistenceData.previousSubmitDataValue, this.fullDatabaseInfosRally)
    .pipe(first())
    .subscribe( result => {
      console.log(result);
      if(activNav){
        this.router.navigate(['dashboard']);
        this.alertService.success('Rally enregistré avec succès');
        return;
      }
    }, err => {
      if(activNav){
        this.alertService.error('Probléme lors de l‘enregistrement du rally !');
        return;
      }
      console.log(err);
    });

  }

}
