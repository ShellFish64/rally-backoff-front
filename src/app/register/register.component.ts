import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { BehaviorSubject } from 'rxjs';
//import { UserService } from '@/_services';
import { UserService } from '@/_services/user.service';
import { Router } from '@angular/router';
import { AlertService } from '@/_services';
import { AuthenticationService } from '@/_services';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  registerFrom: FormGroup;
  loading = false;
  submitted = false;


  /**
   * ICI TOUT LES "GUARDS" :
   * FLAGS d'INPUTS desquel DÉPENDENT la VALIDATION des champs et l'AFFICHAGE de MESSAGES.
   * Seul le LOCKER commence a FALSE pour passer a TRUE.
   * Les GAURDS de validation formulaire passent a FALSE si une ERREUR est détectée.
   */
  
  nameErrorLength = new BehaviorSubject<boolean>(undefined);
  nameErrorChar = new BehaviorSubject<boolean>(undefined);

  mailError = new BehaviorSubject<boolean>(undefined);

  lengthTelErr = new BehaviorSubject<boolean>(undefined);
  numberTelErr = new BehaviorSubject<boolean>(undefined);

  passwordLength = new BehaviorSubject<boolean>(undefined);

  repassword = new BehaviorSubject<boolean>(undefined);

  guardError = new BehaviorSubject<boolean>(false);



  constructor(
    private authenticationService: AuthenticationService,
    private alertService: AlertService,
    private formBuilder: FormBuilder,
    private userService: UserService,
    private router: Router
  ) { 
    if(this.authenticationService.currentUserToken){
      //console.log(this.authenticationService.currentUserToken);  
      //this.router.navigate(['/dashboard']);
    } 
  }

  ngOnInit() {
    this.registerFrom = this.formBuilder.group({
      name: ['', [Validators.required, Validators.minLength(3)]],
      mail: ['', Validators.required],
      password: ['', [Validators.required, Validators.minLength(5)]],
      repassword: ['', Validators.required],
      tel: ['', Validators.required],
    });
  }

  get f() { return this.registerFrom.controls; }

  verifyMail(){
    let mail = this.f.mail.value;
    console.log(this.f.mail.value);
    if(mail !== undefined && mail.length === 0){
      this.mailError.next(undefined);
    }
    let regexMail = new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
    if(!(regexMail.test(mail))){
      //if(mail.length === 0) return;
      this.mailError.next(false);
    } else {
      this.mailError.next(true);
    }

    this.analyze();
    return 
  }

  verifyName(){
    let name = this.f.name.value;
    let regExName = new RegExp(/[^a-zA-Z0-9 ]/);
    if(name !== undefined && name.length === 0){
      this.nameErrorChar.next(undefined);
      this.nameErrorLength.next(undefined);
    }
    if(name.length >= 4 && name.length <= 40){
      this.nameErrorLength.next(true);
    } else {
      this.nameErrorLength.next(false);
    }
    if(!regExName.test(name)) { 
      this.nameErrorChar.next(true);
    } else {
      this.nameErrorChar.next(false);
    }

    this.analyze();
    return;
  }

    verifyTel(){
      let tel = this.f.tel.value;
      console.log('run vereifname')
      console.log(tel);
      let regExTel = new RegExp(/[^[0-9]+$]/);
      if(tel !== undefined && tel.length === 0){
        this.numberTelErr.next(undefined);
        this.lengthTelErr.next(undefined);
      }
      if(tel.length === 10){
        this.lengthTelErr.next(true);
      } else {
        this.lengthTelErr.next(false);
      }
      if(!regExTel.test(tel)) { 
        console.log('good tel')
        this.numberTelErr.next(true);
      } else {
        console.log('bad tel');
        this.numberTelErr.next(false);
      }
      this.analyze();
      return;
  }
  verifyPass(){
    let password = this.f.password.value;
    if(password !== undefined && password.length === 0){
      this.passwordLength.next(undefined);
    }
    if(!(password.length < 8)){
      this.passwordLength.next(true);
    } else {
      this.passwordLength.next(false);
    }
    this.analyze();
    return;
}

verifyRepass(){
  let repassword = this.f.repassword.value;
  if(repassword !== undefined && repassword.length === 0){
    this.repassword.next(undefined);
  }
  if(repassword === this.f.password.value){
    this.repassword.next(true);
  } else {
    this.repassword.next(false);
  }

  this.analyze();
  return;
}

analyze(){
  if (this.nameErrorLength.value && this.nameErrorChar.value && this.mailError.value &&
    this.lengthTelErr.value && this.numberTelErr.value && this.passwordLength.value
    && this.repassword.value) {
      this.guardError.next(true);
    } else { 
      this.guardError.next(false);
    }
}

  onSubmit() {
    this.submitted = true;
    if (this.registerFrom.invalid) {
      return;
    }
    this.loading = true;
    this.userService.register(this.registerFrom.value)
      .pipe(first())
      .subscribe(
        data => {
          //alert(data);
          //console.log('nn' + data);
          this.router.navigate(['/login']);
          this.alertService.success('Vous êtes à présent inscrit');
        },
        error => {
          this.loading = false;
          console.log(error);
          if(error.status === 200){
            this.router.navigate(['/login']);
            this.alertService.success('Vous êtes à présent inscrit');
          } else {
            this.alertService.error('Erreur lors de l\'inscription');
          }
        });
  }

}
