import { Injectable } from "@angular/core";
import { HttpInterceptor, HttpRequest, HttpHandler } from '@angular/common/http';
import { AuthenticationService } from '@/_services';
import { Observable } from 'rxjs';


@Injectable()
export class JwtInterceptor implements HttpInterceptor {
    constructor(private authenticationService: AuthenticationService) {}

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<any> {
        //localStorage.currentUser.tokentest = "test";
        let currentUser = this.authenticationService.currentUserToken;
        if(currentUser && currentUser.accessToken){
            //localStorage.currentUser.tokentest = "test";
            request = request.clone({
                headers: request.headers.set('x-access-token', currentUser.accessToken)
            })
        }
        return next.handle(request);
    }
}