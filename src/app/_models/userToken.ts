export class UserToken {
    auth: string;
    accessToken: string;
}