import { Routes, RouterModule } from '@angular/router';
import { RegisterComponent } from './register/';
import { LoginComponent } from './login';
import { HomeComponent } from './home';
import { AuthGuard } from './_helpers';
import { NoAuthGuard } from './_helpers';
import { PersistenceGuard } from './_helpers';
import { CreationStepComponent } from '@/creation-step';
import { Step1Component } from '@/step1';
import { Step2Component } from '@/step2';
import { Step3Component } from '@/step3';
import { Step4Component } from '@/step4';
import { Step5Component } from '@/step5';
import { Step6Component } from '@/step6';
import { DetailsComponent } from '@/details';
import { DashComponent } from '@/dash'; 
import { ResetpwdComponent } from './resetpwd/resetpwd.component';
import { UpdatePasswordComponent } from './update-password/update-password.component';
import { AppComponent } from './app.component';

const routes: Routes = [
  //{ path: '', component: AppComponent },
  { path: 'dashboard', component:  DashComponent, canActivate: [AuthGuard], children: [
    { path: '', component: HomeComponent },
    { path: 'details/:id', component: DetailsComponent, canActivate: [AuthGuard],/*canActivateChild: [PersistenceChildGuard],*/ children: [
        { path: 'edit-step/1', component: Step1Component/*, canActivate: [PersistenceGuard]*/ },
        { path: 'edit-step/2', component: Step2Component/*, canActivate: [PersistenceGuard] */},
        { path: 'edit-step/3', component: Step3Component/*, canActivate: [PersistenceGuard] */},
        { path: 'edit-step/4', component: Step4Component/*, canActivate: [PersistenceGuard] */},
        { path: 'edit-step/5', component: Step5Component/*, canActivate: [PersistenceGuard] */},
        { path: 'edit-step/6', component: Step6Component/*, canActivate: [PersistenceGuard] */}
        ] 
      },
  ]
},
  { path: 'register', component: RegisterComponent,  canActivate: [NoAuthGuard]},
  { path: 'login', component: LoginComponent,  canActivate: [NoAuthGuard]},
  { path: 'reset/:token', component: UpdatePasswordComponent,  canActivate: [NoAuthGuard]},
  { path: 'reset_pwd', component: ResetpwdComponent, canActivate: [NoAuthGuard]},
  { path: 'creation-step', component: CreationStepComponent, canActivate: [AuthGuard], children: [
    { path: '1', component: Step1Component /*,canActivate: [PersistenceGuard]*/},
    { path: '2', component: Step2Component /*, canActivate: [PersistenceGuard]*/},
    { path: '3', component: Step3Component, /*canActivate: [PersistenceGuard]*/},
    { path: '4', component: Step4Component, canActivate: [PersistenceGuard] },
    { path: '5', component: Step5Component, canActivate: [PersistenceGuard] },
    { path: '6', component: Step6Component, /*canActivate: [PersistenceGuard]*/ }
    ] 
  },

  { path: '**', redirectTo: 'dashboard'}
];

export const appRoutingModule = RouterModule.forRoot(routes);
