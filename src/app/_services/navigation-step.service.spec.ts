import { TestBed } from '@angular/core/testing';

import { NavigationStepService } from './Navigation-step.service';

describe('NavigationStepServiceStepService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: NavigationStepService = TestBed.get(NavigationStepService);
    expect(service).toBeTruthy();
  });
});
