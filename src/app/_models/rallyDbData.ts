export interface RallyDbData {
    createdAt?: string;
    deletedAt?: null;
    hashLink?: string;
    id?: number;
    id_manager?: number;
    lat?: null;
    lng?: null;
    name?: string;
    updatedAt?: string;
    ville?: null;
}