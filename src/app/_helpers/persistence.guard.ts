import { Injectable } from '@angular/core';
import { Router, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, CanActivate } from '@angular/router';
import { Observable } from 'rxjs';
import { PersistenceFormsService, UserService } from '@/_services';
import { map } from 'rxjs/operators';
import { WayPoint, Rally } from '@/_models';

@Injectable({
  providedIn: 'root'
})
export class PersistenceGuard implements  CanActivate{
  waypoint: WayPoint[];
  rally: Rally;
  constructor(
    private router: Router,
    private userService: UserService,
    private peristenceData: PersistenceFormsService
  ){console.log('guard called')}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if(route.url[0]['path'] === "2"){
      return this.userService.getUser().pipe(
        map(
        (user, error) => {
          if(user) {
            this.rally = this.peristenceData.previousSubmitDataValue[0];
            if(this.rally.expiration !== "" && this.rally.expiration !== undefined && this.rally.name !== "" && this.rally.name !==  undefined){
              console.log('there is wpts');
              return true;
            } else {
              this.router.navigate(['/dashboard'])
              return false;
            }
          } 
          if(error) {
            this.router.navigate(['/login'])
          }
          return false;
        }));
    }
    /*if(route.url[0]['path'] === "1"){
      console.log('call');
      return this.userService.getUser().pipe(
        map(
        (user, error) => {
          if(user) {
              if(route.parent.routeConfig.path === 'creation-step'){
                this.peristenceData.tester();
                console.log(this.peristenceData.getToken);
                if(this.peristenceData.getToken !== false) return true;
                if(!this.peristenceData.getToken){ this.router.navigate(['dashboard']); return false}
              }
              
          }  
          if(error) {
            this.router.navigate(['/dashboard'])
          }
          return false;
        }));
    }*/
    return this.userService.getUser().pipe(
      map(
      (user, error) => {
        if(user) {
          this.waypoint = this.peristenceData.previousSubmitDataValue[1];
          if(this.waypoint.length > 0){
            console.log('there is wpts');
            return true;
          } else {
            this.router.navigate(['/dashboard'])
            return false;
          }
        } 
        if(error) {
          this.router.navigate(['/login'])
        }
        return false;
      }));
  }
}
